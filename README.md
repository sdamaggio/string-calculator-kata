# String Calculator Kata
The following is a TDD Kata, an exercise in coding, refactoring and test-first, that you should apply daily for at least 15-30 minutes.

### Compile and run
In order to compile and run the program:

- open it using Visual Studio (https://visualstudio.microsoft.com/downloads/)
or Rider (https://www.jetbrains.com/rider/).

- Press Compile.

- Press Test > Run All Tests 