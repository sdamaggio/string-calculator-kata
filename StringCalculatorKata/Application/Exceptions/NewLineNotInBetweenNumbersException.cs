﻿using System;

namespace StringCalculatorKata.Application.Exceptions
{
    [Serializable]
    class NewLineNotInBetweenNumbersException : Exception
    {
        public NewLineNotInBetweenNumbersException()
            : base("newline is not in between numbers")
        {

        }
    }
}
