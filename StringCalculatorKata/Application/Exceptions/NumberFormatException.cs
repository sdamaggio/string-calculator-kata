﻿using System;

namespace StringCalculatorKata.Application.Exceptions
{
    public sealed class NumberFormatException: ArgumentException
    {
        public NumberFormatException(string number) : base(number) {}
    }
}