﻿using System;

namespace StringCalculatorKata.Application.Exceptions
{
    [Serializable]
    class NegativeNumbersNotAllowedException : Exception
    {
        public NegativeNumbersNotAllowedException(int value)
            : base($"negatives not allowed: {value}")
        {

        }
    }
}
