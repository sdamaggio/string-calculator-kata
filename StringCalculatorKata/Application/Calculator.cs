﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using StringCalculatorKata.Application.Exceptions;

namespace StringCalculatorKata.Application
{
    internal class Calculator
    {
        private static readonly Regex NewLineStartRegex = new Regex("^\n.*$");
        private static readonly Regex NewLineEndRegex = new Regex("^.*\n$");

        private static readonly char[] DefaultDelimiters = {','};
        private const string CustomDelimiterMarker = "//";
        private const int TooBigNumberTreshold = 1000;

        public int Add(string numbers) =>
            ParseNumbers(numbers).Sum(a => a);

        private IEnumerable<int> ParseNumbers(string numbers)
        {
            var delimiters = ParseDelimiter(numbers, out numbers);

            EnsureNumbersAreValid(numbers);

            var addends = numbers
                .Split('\n')
                .SelectMany(v => v.Split(delimiters, StringSplitOptions.RemoveEmptyEntries));

            foreach (var addend in addends)
            {
                if (!int.TryParse(addend, out var intAddend))
                {
                    throw new NumberFormatException(addend);
                }

                if (intAddend < 0)
                {
                    throw new NegativeNumbersNotAllowedException(intAddend);
                }

                if (intAddend > TooBigNumberTreshold)
                {
                    continue;
                }

                yield return intAddend;
            }
        }

        private static void EnsureNumbersAreValid(string numbers)
        {
            if (NewLineStartRegex.IsMatch(numbers))
            {
                throw new ArgumentOutOfRangeException(nameof(numbers), "Input string should not start with a newline");
            }

            if (NewLineEndRegex.IsMatch(numbers))
            {
                throw new ArgumentOutOfRangeException(nameof(numbers), "Input string should not end with a newline");
            }
        }

        private static char[] ParseDelimiter(string input, out string numbers)
        {
            if (!input.StartsWith(CustomDelimiterMarker))
            {
                numbers = input;
                return DefaultDelimiters;
            }

            var customDelimiter = input.Substring(CustomDelimiterMarker.Length, 1);
            numbers = input.Substring(CustomDelimiterMarker.Length +
                                      "\n".Length
                                      + 1);

            return new[] {customDelimiter[0]};
        }
    }
}