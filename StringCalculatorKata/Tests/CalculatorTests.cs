﻿using System;
using StringCalculatorKata.Application;
using StringCalculatorKata.Application.Exceptions;
using Xunit;

namespace StringCalculatorKata.Tests
{
    public class CalculatorTests
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("1", 1)]
        [InlineData("1,2", 3)]
        [InlineData("1\n2,3", 6)]
        [InlineData("//;\n1;2", 3)]
        [InlineData("2,1001", 2)]
        public void Should_AddNumbers_When_InputIsCorrect(string input, int expected)
        {
            // Arrange
            var calculator = new Calculator();
            
            // Act
            var result = calculator.Add(input);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("1,4,-1", "negatives not allowed: -1")]
        public void Should_ThrowException_When_InputContainsNegativeNumbers(string input, string expected)
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var exception = Record.Exception(() => calculator.Add(input));

            // Assert
            Assert.NotNull(exception);
            Assert.IsType<NegativeNumbersNotAllowedException>(exception);
            Assert.Equal(expected, exception.Message);
        }
        
        [Theory]
        [InlineData("\n1,4,-1", "Input string should not start with a newline (Parameter 'numbers')")]
        [InlineData("1,4,-1\n", "Input string should not end with a newline (Parameter 'numbers')")]
        public void Should_ThrowException_When_NewLineIsNotInBetweenNumbers(string input, string expected)
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var exception = Record.Exception(() => calculator.Add(input));

            // Assert
            Assert.NotNull(exception);
            Assert.IsType<ArgumentOutOfRangeException>(exception);
            Assert.Equal(expected, exception.Message);
        }
    }
}